#!/bin/bash
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
cd /tmp
git clone https://github.com/torvalds/linux
cd linux
git checkout v5.4-rc4
cp $SCRIPTPATH/../../opt/kernel.conf /tmp/linux/.config
patch -p1 < $SCRIPTPATH/../patches/lpss.patch
patch -p1 < $SCRIPTPATH/../patches/hid.patch
make -j4
make -j4 modules
sudo make modules_install
sudo make install
